# terraform-gitlab-aws-oidc

This module configures multiple OIDC roles from GitLab CI to an AWS OpenID Connect Identity Provider.

<!-- BEGIN_TF_DOCS -->


## Resources

| Name | Type |
|------|------|
| [aws_iam_openid_connect_provider.item](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_openid_connect_provider) | resource |
| [aws_iam_role.item](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role) | resource |
| [aws_iam_policy_document.item](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/data-sources/iam_policy_document) | data source |
| [tls_certificate.item](https://registry.terraform.io/providers/hashicorp/tls/latest/docs/data-sources/certificate) | data source |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_aud_value"></a> [aud\_value](#input\_aud\_value) | GitLab Audience (aud) | `string` | `"https://gitlab.com"` | no |
| <a name="input_gitlab_url"></a> [gitlab\_url](#input\_gitlab\_url) | GitLab URL for use with OIDC | `string` | `"https://gitlab.com"` | no |
| <a name="input_oidc_roles"></a> [oidc\_roles](#input\_oidc\_roles) | One or more OIDC roles for this GitLab OIDC provider | <pre>map(object({<br/>    role_name                  = string                     # Role Name for IAM Role<br/>    role_path                  = string                     # Role Path for IAM Role<br/>    match_field                = string                     # OIDC: GitLab match_field: use sub for subject, aud for audience.<br/>    match_value                = list(string)               # OIDC: GitLab match_value<br/>    policy_arns                = list(string)               # Policies attached to this role<br/>    assume_role_principal_arns = optional(list(string), []) # Additional principals allowed to assume the role. Useful for local development.<br/>    max_session_duration       = optional(number, 3600)     # Session duration maximum. Defaults to 1 hour<br/>  }))</pre> | n/a | yes |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_aws_iam_role_arns"></a> [aws\_iam\_role\_arns](#output\_aws\_iam\_role\_arns) | ARN for the IAM roles created for OIDC authentication |
| <a name="output_aws_iam_role_names"></a> [aws\_iam\_role\_names](#output\_aws\_iam\_role\_names) | Name of the IAM roles created for OIDC authentication |
<!-- END_TF_DOCS -->

## Example Configuration

Here is an example of using the OIDC module to setup two OIDC configurations for GitLab.com -> AWS.

```terraform
module "oidc_gitlab_com" {
  source  = "gitlab.com/gitlab-com/terraform-gitlab-aws-oidc/local"
  version = "1.1.0" # Note: update this to reflect the current release...

  oidc_roles = {
    # Allow all branches of gitlab-com/gl-infra/project to obtain ReadOnlyAccess
    branch = {
      role_name   = "GitLabCIOIDC-Project-Branch"
      role_path   = "/ci/"
      match_field = "sub"
      match_value = ["project_path:gitlab-com/gl-infra/project:ref_type:branch:ref:*"]
      policy_arns = ["arn:aws:iam::aws:policy/ReadOnlyAccess"]

      assume_role_principal_arns = []
    }

    # Allow the main branch of gitlab-com/gl-infra/project to obtain AdministratorAccess
    main = {
      role_name   = "GitLabCIOIDC-Project-Main"
      role_path   = "/ci/"
      match_field = "sub"
      match_value = ["project_path:gitlab-com/gl-infra/project:ref_type:branch:ref:main"]
      policy_arns = ["arn:aws:iam::aws:policy/AdministratorAccess"]

      assume_role_principal_arns = []
    }
  }
}
```

## Bootstrapping Process

Since this module is used to establish a connection from GitLab CI/CD, often used for Terraform to provision the project, there is sometimes a chicken-and-egg situation in which OIDC needs to be setup via Terraform, but Terraform needs OIDC to be setup.

This section is a guide to the recommended approach to dealing with this through an environment bootstrapping process.

If you're looking for a detailed example of this process in action, the Amp project has [a good example of this bootstrapping process](https://gitlab.com/gitlab-com/gl-infra/gitlab-dedicated/amp/-/merge_requests/796).

### Bootstrapping Step 1: Create an `oidc` Module

In your project, create a module named `oidc`, containing only the OIDC module definition.

For this tutorial, we'll assume this is in the `modules/oidc` directory:

This module contains the OIDC definition:


```terraform
# modules/oidc/main.tf
module "oidc_gitlab_com" {
  source  = "gitlab.com/gitlab-com/terraform-gitlab-aws-oidc/local"
  version = "1.1.0" # Note: update this to reflect the current release...

  ...
}
```

### Bootstrapping Step 2: Use the `oidc` Module from your Root Module

From your main or root module, declare an instance of the `oidc` module:

```terraform
# main.tf
module "oidc" {
  source = "modules/oidc"
}
```

### Bootstrapping Step 3: Deploy the `oidc` Module Directly from your local environment

First, ensure that your AWS environment is properly configured and working properly. Running `aws sts get-caller-identity` should return a valid identity.

Use Terraform to deploy the OpenID Connect Identity Provider.

```shell
export AWS_REGION=...

terraform -chdir=modules/oidc init -backend=false -reconfigure
terraform -chdir=modules/oidc apply
```

### Bootstrapping Step 4: Import the OpenID Connect Provider In Your Main Terraform Module

To import the resource, either use an `import` block or import the resource manually. Note that import block only supports interpolatable id from [Terraform version 1.6](https://github.com/hashicorp/terraform-schema/issues/239).

#### Importing the OIDC Provider using an `import` block

1. The `to` address will be `module.oidc_gitlab_com.aws_iam_openid_connect_provider.item`
2. The `id` will be `arn:aws:iam::<AWS_ACCOUNT_ID>:oidc-provider/gitlab.com` where `AWS_ACCOUNT_ID` is the 12 digit AWS Account ID.

```
import {
  to = module.oidc_gitlab_com.aws_iam_openid_connect_provider.item
  # ID as per https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_openid_connect_provider#import
  id = "arn:aws:iam::<AWS_ACCOUNT_ID>:oidc-provider/gitlab.com"
}
```

#### Importing the OIDC Provider using an `import` block

Alternatively, add a manual import step to your CI/CD script. It is recommended to use [`terra-transformer import`](https://gitlab.com/gitlab-com/gl-infra/terra-transformer) tool for this:

```shell
terraform plan -out="plan.tfplan"

terraform show --json "plan.tfplan" >"plan.tfplan.json"

# Import items created during the bootstrap process
terra-transformer import --plan-file "plan.tfplan.json" 'module.oidc.module.oidc_gitlab_com.aws_iam_openid_connect_provider.item'
```

Once this is done, proceed with `terraform apply` as usual. The bootstrapped OIDC provider will now be included in the main Terraform state.

Any further changes can be done directly through the Terraform module.

## Hacking on terraform-gitlab-aws-oidc

### Preparing your Environment

1. Follow the developer setup guide at <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docs/developer-setup.md>.
1. Clone this project
1. Run `scripts/prepare-dev-env.sh`
