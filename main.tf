terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= 4.0"
    }
    tls = {
      source  = "hashicorp/tls"
      version = ">= 4.0"
    }
  }
  required_version = "~> 1.3"
}

data "tls_certificate" "item" {
  url = "${var.gitlab_url}/oauth/discovery/keys"
}

resource "aws_iam_openid_connect_provider" "item" {
  url             = var.gitlab_url
  client_id_list  = [var.aud_value]
  thumbprint_list = data.tls_certificate.item.certificates[*].sha1_fingerprint
}

data "aws_iam_policy_document" "item" {
  for_each = var.oidc_roles

  statement {
    actions = ["sts:AssumeRoleWithWebIdentity"]

    principals {
      type        = "Federated"
      identifiers = [aws_iam_openid_connect_provider.item.arn]
    }
    condition {
      test     = "StringLike"
      variable = "${aws_iam_openid_connect_provider.item.url}:${each.value.match_field}"
      values   = each.value.match_value
    }
  }

  # Only add this statement if the caller has added additional
  # principals
  dynamic "statement" {
    for_each = each.value.assume_role_principal_arns

    content {
      actions = ["sts:AssumeRole"]
      principals {
        type        = "AWS"
        identifiers = [statement.value]
      }
    }
  }
}

resource "aws_iam_role" "item" {
  for_each = var.oidc_roles

  name                = each.value.role_name
  description         = "GitLabCI with OIDC"
  path                = each.value.role_path
  assume_role_policy  = data.aws_iam_policy_document.item[each.key].json
  managed_policy_arns = each.value.policy_arns

  max_session_duration = each.value.max_session_duration
}
