output "aws_iam_role_arns" {
  value       = { for k, v in var.oidc_roles : k => aws_iam_role.item[k].arn }
  description = "ARN for the IAM roles created for OIDC authentication"
}

output "aws_iam_role_names" {
  value       = { for k, v in var.oidc_roles : k => aws_iam_role.item[k].name }
  description = "Name of the IAM roles created for OIDC authentication"
}
