variable "gitlab_url" {
  description = "GitLab URL for use with OIDC"
  type        = string
  default     = "https://gitlab.com"
  nullable    = false
}

variable "aud_value" {
  description = "GitLab Audience (aud)"
  type        = string
  default     = "https://gitlab.com"
  nullable    = false
}

variable "oidc_roles" {
  description = "One or more OIDC roles for this GitLab OIDC provider"
  type = map(object({
    role_name                  = string                     # Role Name for IAM Role
    role_path                  = string                     # Role Path for IAM Role
    match_field                = string                     # OIDC: GitLab match_field: use sub for subject, aud for audience.
    match_value                = list(string)               # OIDC: GitLab match_value
    policy_arns                = list(string)               # Policies attached to this role
    assume_role_principal_arns = optional(list(string), []) # Additional principals allowed to assume the role. Useful for local development.
    max_session_duration       = optional(number, 3600)     # Session duration maximum. Defaults to 1 hour
  }))
}
